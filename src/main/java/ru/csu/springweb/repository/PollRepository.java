package ru.csu.springweb.repository;

import org.springframework.data.repository.CrudRepository;
import ru.csu.springweb.entity.Poll;

import java.time.LocalDate;

public interface PollRepository extends CrudRepository<Poll, Long> {
    Poll findByStartDateBetween(LocalDate startDate, LocalDate endDate);
}
