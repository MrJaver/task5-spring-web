package ru.csu.springweb.mapper;

import org.mapstruct.Mapper;
import ru.csu.springweb.dto.PollDto;

@Mapper(componentModel = "spring")
public interface PollMapper extends AbstractMapper<PollDto, ru.csu.springweb.entity.Poll> {

}
