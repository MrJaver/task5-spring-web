package ru.csu.springweb.mapper;

import org.mapstruct.Mapper;
import ru.csu.springweb.dto.QuestionDto;
import ru.csu.springweb.entity.Question;

@Mapper(componentModel = "spring")
public interface QuestionMapper extends AbstractMapper<Question, QuestionDto> {

}