package ru.csu.springweb.service;

import ru.csu.springweb.dto.PollDto;
import ru.csu.springweb.dto.QuestionDto;
import ru.csu.springweb.model.PollModel;

import java.time.LocalDate;
import java.util.List;

public interface PollService {
    void createPoll(PollDto pollDto);

    void deletePoll(long id);

    PollModel getPoll(long id);

    void addQuestionToPoll(long pollId, QuestionDto questionDto);

    void deleteQuestion(long id);

    List<PollModel> findPollsByDate(LocalDate start, LocalDate end);

}
