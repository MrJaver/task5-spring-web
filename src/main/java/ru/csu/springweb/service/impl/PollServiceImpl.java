package ru.csu.springweb.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.csu.springweb.dto.PollDto;
import ru.csu.springweb.dto.QuestionDto;
import ru.csu.springweb.entity.Poll;
import ru.csu.springweb.mapper.PollMapper;
import ru.csu.springweb.mapper.QuestionMapper;
import ru.csu.springweb.model.PollModel;
import ru.csu.springweb.model.QuestionModel;
import ru.csu.springweb.repository.PollRepository;
import ru.csu.springweb.repository.QuestionsRepository;
import ru.csu.springweb.service.PollService;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class PollServiceImpl implements PollService
{

    private final PollRepository pollRepository;
    private final QuestionsRepository questionsRepository;
    private final PollMapper pollMapper;
    private final QuestionMapper questionMapper;

    @Override
    public void createPoll(PollDto pollDto) {
        pollRepository.save(pollMapper.mapSource(pollDto));
    }

    @Override
    public void deletePoll(long id) {
        pollRepository.deleteById(id);
    }

    @Override
    public PollModel getPoll(long id) {
        return pollRepository.findById(id)
                .map(p -> PollModel
                        .builder()
                        .id(p.getId())
                        .pollName(p.getPollName())
                        .active(p.getActive())
                        .startDate(p.getStartDate())
                        .endDate(p.getEndDate())
                        .questions(p.getQuestions().stream().map(q -> QuestionModel
                                .builder()
                                .question(q.getQuestion())
                                .number(q.getNumber())
                                .build()).collect(Collectors.toList()))
                        .build())
                .orElseThrow(() -> {
                    throw new EntityNotFoundException("Запрашеваемый ресурс не найден");
                });
    }

    @Override
    @Transactional
    public void addQuestionToPoll(long pollId, QuestionDto questionDto) {
        var poll = pollRepository.findById(pollId).orElseThrow(() -> {
            throw new EntityNotFoundException("");
        });

        var question = questionMapper.mapTarget(questionDto);

        question.setPoll(poll);
        questionsRepository.saveAndFlush(question);

    }

    @Override
    public void deleteQuestion(long id) {
        var question = questionsRepository.findById(id)
                .orElseThrow(() -> {
                    throw new EntityNotFoundException();
                });

        questionsRepository.delete(question);
    }

    @Override
    public List<PollModel> findPollsByDate(LocalDate start, LocalDate end) {
        var polls = pollRepository.findAll();
        var filteredPolls = new ArrayList<Poll>();

        polls.forEach(poll -> {
            if (poll.getStartDate().isAfter(start) && poll.getEndDate().isBefore(end)) {
                filteredPolls.add(poll);
            }
        });

        return filteredPolls.stream().map(poll -> PollModel
                .builder()
                .id(poll.getId())
                .pollName(poll.getPollName())
                .active(poll.getActive())
                .startDate(poll.getStartDate())
                .endDate(poll.getEndDate())
                .build()).collect(Collectors.toList());
    }
}
