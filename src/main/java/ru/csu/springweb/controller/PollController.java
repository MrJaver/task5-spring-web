package ru.csu.springweb.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ru.csu.springweb.dto.PollDto;
import ru.csu.springweb.dto.PollFiltersDto;
import ru.csu.springweb.dto.QuestionDto;
import ru.csu.springweb.model.PollModel;
import ru.csu.springweb.service.PollService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/polls")
public class PollController
{

    private final PollService pollService;

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    public void createPoll(@RequestBody @Valid PollDto pollDto) {
        pollService.createPoll(pollDto);
    }

    @GetMapping("{id}")
    public PollModel getPollById(@PathVariable long id) {
        return pollService.getPoll(id);
    }

    @GetMapping("search")
    public List<PollModel> searchPollsByFilter(@RequestParam PollFiltersDto pollFilters) {
        return pollService.findPollsByDate(pollFilters.getStartDate(), pollFilters.getEndDate());
    }

    @DeleteMapping("{id}")
    @ResponseStatus(code = HttpStatus.OK)
    public void deletePoll(@PathVariable long id) {
        pollService.deletePoll((int) id);
    }

    @PostMapping("{pollId}/questions")
    @ResponseStatus(code = HttpStatus.CREATED)
    public void addQuestionToPoll(@PathVariable long pollId, @RequestBody QuestionDto questionDto) {
        pollService.addQuestionToPoll(pollId, questionDto);
    }
}
